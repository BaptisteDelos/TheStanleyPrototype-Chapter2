# The Stanley Prototype - Chapter Two

## Foreword

This project is the logical follow-up of the <a href="https://gitlab.com/BaptisteDelos/TheStanleyPrototype" target="_blank">Stanley Prototype</a> Master project. If it actually allows to capitalize on learned notions from my <a href="http://departement-informatique.univ-tlse3.fr/master-igai" target="_blank">formation</a>, the long-term intention of this project is to provide a small software base for existing real-time rendering techniques development, and as far as possible, further experimentations.  
Hoping the reference this appellation refers to will make sense in a near future.

## Dependencies

* CMake (*3.11.2 or higher*)
* glbinding (*as submodule*)
* GLFW (*as submodule*)
* SOIL2 (*as submodule*)
* Eigen (*as submodule*)
* libigl (*as submodule*)

## Getting submodules

```
git submodule init
git submodule update
```

## Building

```
mkdir build && cd build
cmake ..
make -j $(nproc)
```