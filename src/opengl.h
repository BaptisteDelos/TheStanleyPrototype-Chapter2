#ifndef GLERROR_H
#define GLERROR_H

#define GLFW_INCLUDE_NONE

#include <glbinding/gl43core/gl.h>
#include <glbinding/Binding.h>

using namespace gl43core;

#include <GLFW/glfw3.h>

#include <iostream>


/**
 * Redirect OpenGL errors, if any, on stderr
 */
inline void _check_gl_error(const char *file, int line) {
    GLenum error = glGetError();
    if (error != GL_NO_ERROR)
        std::cerr << "OpenGL error: " << int(error) << " in " << file << " at line " << line << std::endl;
}

#define glCheckError() _check_gl_error(__FILE__,__LINE__)

#endif // GLERROR_H
