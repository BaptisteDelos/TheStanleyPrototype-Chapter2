#include "cylinder.h"

Cylinder::Cylinder(Vector3f base, Vector3f axis, float radius, float length, int subdiv1, int subdiv2) : TriangleMesh()
{
    std::vector<Eigen::Vector3f> positions;
    std::vector<Eigen::Vector3f> normals;
    std::vector<Eigen::Vector2f> texcoords;
    std::vector<Eigen::Vector3i> indices;
    Vector3f x = Vector3f(0,1,0); //orthogonal(axis);
    Vector3f y = axis.cross(x);

    for(int i = 0; i < subdiv2; i++)
    {
        float offset = float(i) / float(subdiv2 - 1);
        float offset2 = (offset - 0.5) * length;

        for(int j = 0; j < subdiv1; j++)
        {
            Vector3f pos, normal;
            Vector2f uv;

            float angle = 2. * M_PI * float(j) / float(subdiv1);
            pos = base + offset2 * axis + radius * cos(angle) * x + radius * sin(angle) * y;
            normal = cos(angle) * x + sin(angle) * y;
            normal.normalize();
            uv = Vector2f(offset, float(j) / float(subdiv1 - 1));

            positions.emplace_back(pos);
            normals.emplace_back(normal);
            texcoords.emplace_back(uv);
        }
    }

    for(unsigned int i = 0; i < subdiv2 - 1; i++)
    {
        for(unsigned int j = 0; j < subdiv1; j++)
        {
            indices.emplace_back(Eigen::Vector3i(i * subdiv1 + j,
                                                 i * subdiv1 + j + subdiv1,
                                                 i * subdiv1 + (j + 1)%subdiv1));
            indices.emplace_back(Eigen::Vector3i(i * subdiv1 + (j + 1)%subdiv1,
                                                 i * subdiv1 + j + subdiv1,
                                                 i * subdiv1 + (j + 1)%subdiv1 + subdiv1));
        }
    }

    init(2 * subdiv1 * (subdiv2 - 1), indices.data(), positions.size(), positions.data(), normals.data(), texcoords.data());
    initGLBuffers();
    updateGLBuffers();
}
