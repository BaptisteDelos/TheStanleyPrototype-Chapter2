#ifndef SCENE_H
#define SCENE_H

#include "opengl.h"
#include <vector>

/** Simple classe for managing an OpenGL scene
 */
class Scene {

public:
    explicit Scene(int width, int height);
    virtual ~Scene();

    virtual void resize(int width, int height);
    virtual void draw();

    virtual void mouseclick(int button, float xpos, float ypos);
    virtual void mousepressed(GLFWwindow *window, int button, int action, int mods);
    virtual void mousemove(float xpos, float ypos);
    virtual void mousescroll(float xpos, float ypos);
    virtual void keyboardmove(int key, double time);
    virtual bool keyboard(unsigned int key);
    virtual bool keyevent(int key, int scancode, int action, int mods);


    void toggledrawmode();
    void setOpenGLFrameBuffer(GLuint fbo);

protected:
    // Width and heigth of the viewport
    int _width;
    int _height;
    GLuint _defaultFramebufferObject;

    // Rendering mode (true is filled, false is wireframed
    bool _drawfill;
};


#endif // SCENE_H
