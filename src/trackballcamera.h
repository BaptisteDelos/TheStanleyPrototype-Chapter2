#ifndef TRACKBALLCAMERA_H
#define TRACKBALLCAMERA_H

#include "opengl.h"
#include <Eigen/Core>
#include <Eigen/Dense>

#define deg2rad(x)      float(M_PI)*(x)/180.f

class TrackBallCamera
{
public:
    TrackBallCamera(Eigen::Vector3f position, Eigen::Vector3f target, Eigen::Vector4i viewport, float fov=45.f, float near=0.1f, float far=200.0f, float motionSensitivity=0.2f, float scrollSensitivity=1.5f);

    Eigen::Matrix4f viewMatrix() const;
    Eigen::Matrix4f projectionMatrix() const;

    void setViewport(const Eigen::Vector4i &viewport);

    void processMouseClick(int button, GLfloat xpos, GLfloat ypos);
    void processMouseMovement(int button, GLfloat xpos, GLfloat ypos);
    void processMouseScroll(GLfloat yoffset);

private:
    float               _theta;
    float               _phi;
    float               _radius;
    Eigen::Vector3f     _target;

    Eigen::Vector4i     _viewport;
    float               _fov;
    float               _near;
    float               _far;

    // Mouse event parameters
    int                 _button;
    GLfloat             _mousexpos;
    GLfloat             _mouseypos;

    float               _motionSensivity;
    float               _scrollSensivity;

    Eigen::Matrix4f lookAt(Eigen::Vector3f from, Eigen::Vector3f to, Eigen::Vector3f worldup=Eigen::Vector3f(0.0f, 1.0f, 0.0f)) const;
    Eigen::Matrix4f perspective(float fovy, float aspectRatio, float near, float far) const;
};

#endif // TRACKBALLCAMERA_H
