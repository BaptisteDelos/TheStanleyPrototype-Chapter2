#ifndef CYLINDER_H
#define CYLINDER_H

#include "trianglemesh.h"


class Cylinder : public TriangleMesh
{
public:
    Cylinder(Vector3f base=Vector3f(0,0,0), Vector3f axis = Vector3f(1,0,0), float radius = .5f, float length = 3., int subdiv1 = 16, int subdiv2 = 32);
};

#endif // CYLINDER_H
