#include "mainscene.h"
#include <iostream>
#include <cmath>
#include <random>
#include <utility>
#include <ctime>
#include <chrono>

#include "cylinder.h"


MainScene::MainScene(int width, int height)
    : Scene(width, height),
      _trackballCamera(nullptr) {

    // Initialize shaders
    initShaders();

    _meshmodel.addMesh(new TriangleMesh(DATA_DIR"/models/teapot.obj"));

    _trackballCamera.reset(new TrackBallCamera(Eigen::Vector3f(0, 0, 50), Eigen::Vector3f(0, 0, 0), Eigen::Vector4i(0.0f, 0.0f, _width, _height)));

    glCheckError();
}

MainScene::~MainScene()
{

}

/* ***********************************************************************************************************************
 */
void MainScene::initShaders()
{
    // Object space shaders
    _programdefault.loadFromFiles(_shaderPaths[DEFAULT]+".vert", _shaderPaths[DEFAULT]+".frag");
    _programdiffuse.loadFromFiles(_shaderPaths[DEFAULT]+".vert", _shaderPaths[BLINN]+".frag");
    _programnormal.loadFromFiles(_shaderPaths[DEFAULT]+".vert", _shaderPaths[NORMAL]+".frag");

    _program = _programdefault;

    glCheckError();
}

/* ***********************************************************************************************************************
 */
void MainScene::initGBuffer()
{
    /* ******** Screen Quad ******** */

    // Fullscreen quad VAO
    glGenVertexArrays(1, &_quadVAO);
    glBindVertexArray(_quadVAO);

    GLuint quadVBO;
    glGenBuffers(1, &quadVBO);
    glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(_quadVertices), &_quadVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void *)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void *)(3 * sizeof(GLfloat)));

    /* ******** SSAO ******** */

    // Generate G-buffer
    glGenFramebuffers(1, &_gBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _gBuffer);

    // Generate position texture
    glGenTextures(1, &_gPosition);
    glBindTexture(GL_TEXTURE_2D, _gPosition);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, _width, _height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _gPosition, 0);

    // Generate normal texture
    glGenTextures(1, &_gNormal);
    glBindTexture(GL_TEXTURE_2D, _gNormal);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, _width, _height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, _gNormal, 0);

    // Generate albedo texture
    glGenTextures(1, &_gAlbedoSpec);
    glBindTexture(GL_TEXTURE_2D, _gAlbedoSpec);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, _gAlbedoSpec, 0);

    GLenum DrawBuffers[3] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
    glDrawBuffers(3, DrawBuffers);

    // Generate Render Buffer Object as depth buffer
    glGenRenderbuffers(1, &_rBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _rBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, _width, _height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _rBuffer);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "ERROR::FRAMEBUFFER::GBUFFER:: Framebuffer is not complete" << std::endl;

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

/* ***********************************************************************************************************************
 */
void MainScene::initScreenRendering()
{
    // Generate screen framebuffer
    glGenFramebuffers(1, &_screenFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, _screenFBO);

    // Generate texture
    glGenTextures(1, &_screenTexture);
    glBindTexture(GL_TEXTURE_2D, _screenTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _screenTexture, 0);

    GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers(1, DrawBuffers);

    // Generate Render Buffer Object as depth buffer
    glGenRenderbuffers(1, &_rScreenBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _rScreenBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, _width, _height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH24_STENCIL8, GL_RENDERBUFFER, _rScreenBuffer);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "ERROR::FRAMEBUFFER::SCREENBUFFER:: Framebuffer is not complete" << std::endl;

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

/* ***********************************************************************************************************************
 */
void MainScene::drawQuad()
{
    glBindVertexArray(_quadVAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}

/* ***********************************************************************************************************************
 */
void MainScene::resize(int width, int height){
    Scene::resize(width, height);
    _trackballCamera->setViewport(Eigen::Vector4i(0, 0, _width, _height));
}

/* ***********************************************************************************************************************
 */
void MainScene::draw() {

    /***** Geometry pass *****/

    glEnable(GL_DEPTH_TEST);
    glViewport(0, 0, _width, _height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.24f, 0.3f, 0.3f, 1.0f);

    if (_drawfill)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    _program.activate();

    glUniformMatrix4fv(_program.getUniformLocation("model"), 1, GL_FALSE, _meshmodel.localTransform().data());
    glUniformMatrix4fv(_program.getUniformLocation("view"), 1, GL_FALSE, _trackballCamera->viewMatrix().data());
    glUniformMatrix4fv(_program.getUniformLocation("projection"), 1, GL_FALSE, _trackballCamera->projectionMatrix().data());

    _meshmodel.draw(_program);

    _program.deactivate();

    glCheckError();
}

void MainScene::mousepressed(GLFWwindow *window, int button, int action, int mods) {
    if(action == GLFW_PRESS)
    {
        if (button == GLFW_MOUSE_BUTTON_1)
        {
            _trackingMode = TM_ROTATE_AROUND;
            _trackballCamera->processMouseClick(button, _mousex, _mousey);
        }
        else if (button == GLFW_MOUSE_BUTTON_3)
        {
            _trackingMode = TM_TRANSLATE;
        }
    }
    else if(action == GLFW_RELEASE)
    {
        _trackingMode = TM_NO_TRACK;
    }
}

/* ***********************************************************************************************************************
 */
void MainScene::mouseclick(int button, float xpos, float ypos) {
    _button = button;
    _mousex = xpos;
    _mousey = ypos;
    _trackballCamera->processMouseClick(_button, _mousex, _mousey);
}

/* ***********************************************************************************************************************
 */
void MainScene::mousemove(float xpos, float ypos) {
    _mousex = xpos;
    _mousey = ypos;
    if (_trackingMode != TM_NO_TRACK) {
        _trackballCamera->processMouseMovement(_button, xpos, ypos);
    }

}

void MainScene::mousescroll(float xpos, float ypos)
{
    (void) xpos;
    _trackballCamera->processMouseScroll(ypos);
}

/* ***********************************************************************************************************************
 */
void MainScene::keyboardmove(int key, double time) {

}

bool MainScene::keyboard(unsigned int key) {
    switch(key) {
        case 'c' :
            _program = _programdefault;
            return true;
        case 'n' :
            _program = _programnormal;
            return true;
        case '0': case '1':
        case '2': case '3':
        case '4': case '5':
        case '6': case '7':
        case '8': case '9':
            return true;
        case 'd':
            _program = _programdiffuse;
            return true;
        case 'w':
            toggledrawmode();
            return true;
        default:
            return false;
    }
}

/* ***********************************************************************************************************************
 */
bool MainScene::keyevent(int key, int scancode, int action, int mods) {

}
