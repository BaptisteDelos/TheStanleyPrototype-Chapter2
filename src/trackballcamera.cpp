﻿#include "trackballcamera.h"


TrackBallCamera::TrackBallCamera(Eigen::Vector3f position, Eigen::Vector3f target, Eigen::Vector4i viewport, float fov, float near, float far, float motionSensitivity, float scrollSensitivity)
    : _theta(std::atan(position.x()/position.z())), _phi(std::acos(position.y()/(position - target).norm())), _radius((position - target).norm()), _target(target),
      _viewport(viewport), _fov(fov), _near(near), _far(far),
      _button(-1), _motionSensivity(motionSensitivity), _scrollSensivity(scrollSensitivity)
{

}

Eigen::Matrix4f TrackBallCamera::viewMatrix() const
{
    Eigen::Vector3f position(
                std::sin(_theta) * std::sin(_phi) * _radius,
                std::cos(_phi) * _radius,
                std::cos(_theta) * std::sin(_phi) * _radius);

    Eigen::Matrix4f view = lookAt(position, _target);

    return view;
}

Eigen::Matrix4f TrackBallCamera::projectionMatrix() const
{
    return perspective(_fov, static_cast<float>(_viewport.z()) / static_cast<float>(_viewport.w()), _near, _far);
}

void TrackBallCamera::setViewport(const Eigen::Vector4i &viewport)
{
    _viewport = viewport;
}

void TrackBallCamera::processMouseClick(int button, GLfloat xpos, GLfloat ypos)
{
    _button = button;
    _mousexpos = xpos;
    _mouseypos = ypos;
}

void TrackBallCamera::processMouseMovement(int button, GLfloat xpos, GLfloat ypos)
{
    (void) button;

    Eigen::Vector2f motionvector(xpos - _mousexpos, ypos - _mouseypos);
    _mousexpos = xpos;
    _mouseypos = ypos;

    switch (_button) {
        case 0:
        {
            _theta -= deg2rad(motionvector.x() * _motionSensivity);
            _phi -= deg2rad(motionvector.y() * _motionSensivity);

            _phi = (_phi >= float(M_PI)) ? float(M_PI) - std::numeric_limits<float>::epsilon() : _phi;
            _phi = (_phi <= 0.0f) ? 0.0f + std::numeric_limits<float>::epsilon() : _phi;

            break;
        }
        default:
            break;
    }
}

void TrackBallCamera::processMouseScroll(GLfloat yoffset)
{
    _radius -= yoffset * _scrollSensivity;
}

Eigen::Matrix4f TrackBallCamera::lookAt(Eigen::Vector3f from, Eigen::Vector3f to, Eigen::Vector3f worldup) const
{
    Eigen::Vector3f forward = (from - to).normalized();
    Eigen::Vector3f right = worldup.cross(forward).normalized();
    Eigen::Vector3f up = forward.cross(right).normalized();

    Eigen::Matrix4f view = Eigen::Matrix4f::Identity();

    view.block<1,3>(0,0) = right;
    view.block<1,3>(1,0) = up;
    view.block<1,3>(2,0) = forward;
    view.block<3,1>(0,3) << -from.dot(right), -from.dot(up), -from.dot(forward);

    return view;
}

Eigen::Matrix4f TrackBallCamera::perspective(float fovy, float aspectRatio, float near, float far) const
{
    Eigen::Matrix4f persp = Eigen::Matrix4f::Zero();

    persp(0, 0) = 1.0f / (std::tan(fovy * 0.5f) * aspectRatio);
    persp(1, 1) = 1.0f / (std::tan(fovy * 0.5f));
    persp(2, 2) = -(far + near) / (far - near);
    persp(2, 3) = -(2.0f * far * near) / (far - near);
    persp(3, 2) = -1.0f;

    return persp;
}
