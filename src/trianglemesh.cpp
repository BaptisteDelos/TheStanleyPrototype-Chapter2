#include "trianglemesh.h"

#include <cassert>
#include <sstream>
#include <cctype>

#include <igl/readOFF.h>
#include <igl/readPLY.h>
#include <igl/readOBJ.h>

TriangleMesh::TriangleMesh()
{

}

TriangleMesh::TriangleMesh(int nTriangles, const Eigen::Vector3i *vertexIndices, int nVertices,
                           const Eigen::Vector3f *positions,
                           const Eigen::Vector3f *normals,
                           const Eigen::Vector2f *texCoords,
                           const Eigen::Matrix4f &objectToWorld)
{
    init(nTriangles, vertexIndices, nVertices, positions, normals, texCoords, objectToWorld);
    initGLBuffers();
    updateGLBuffers();
}

/*
 * Redefine copy constructor to prevent possible GL objects invalidation
 * https://stackoverflow.com/questions/28929452/mesh-class-called-with-default-constructor-not-working-opengl-c
 */
TriangleMesh::TriangleMesh(const TriangleMesh &tm) :
    _objectToWorld(tm._objectToWorld), _vertexIndices(tm._vertexIndices),
    _positions(tm._positions), _normals(tm._normals), _texCoords(tm._texCoords)
{
    initGLBuffers();
    updateGLBuffers();
}

TriangleMesh::TriangleMesh(const std::string &filename)
{
    std::stringstream ss(filename);
    std::string ext;
    while (std::getline(ss, ext, '.'));
    std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);

    if (ext == "off")
    {
        igl::readOFF(filename, _positions, _vertexIndices, _normals);
    }
    else if (ext == "ply")
    {
        igl::readPLY(filename, _positions, _vertexIndices, _normals, _texCoords);
    }
    else if (ext == "obj")
    {
        Eigen::Matrix<float, Eigen::Dynamic, 3, Eigen::RowMajor>    TC;
        Eigen::Matrix<int,   Eigen::Dynamic, 3, Eigen::RowMajor>    FN;
        Eigen::Matrix<float, Eigen::Dynamic, 3, Eigen::RowMajor>    FTC;

        igl::readOBJ(filename, _positions, TC, _normals, _vertexIndices, FTC, FN);
        _texCoords = TC.block(0, 0, TC.rows(), 2);
    }
    else // Unsupported file format
    {
        std::cerr << "Model loading : Unsupported file format (" __FILE__ << " at line " << __LINE__ << ")" << std::endl;
        return;
    }

    initGLBuffers();
    updateGLBuffers();
}

TriangleMesh::~TriangleMesh()
{
    deleteGLBuffers();
}

void TriangleMesh::draw(Shader &shader)
{
    GLuint p_loc = -1, n_loc = -1, uv_loc = -1;

    glBindVertexArray(_vao);

    p_loc = shader.getAttribLocation("iposition");

    glBindBuffer(GL_ARRAY_BUFFER, _pbo);
    glVertexAttribPointer(p_loc, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void *) 0);
    glEnableVertexAttribArray(p_loc);

    if ((n_loc = shader.getAttribLocation("inormal")) != -1)
    {
        glBindBuffer(GL_ARRAY_BUFFER, _nbo);
        glVertexAttribPointer(n_loc, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void *) 0);
        glEnableVertexAttribArray(n_loc);
    }

    if ((uv_loc = shader.getAttribLocation("itexcoord")) != -1)
    {
        glBindBuffer(GL_ARRAY_BUFFER, _tbo);
        glVertexAttribPointer(uv_loc, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (void *) 0);
        glEnableVertexAttribArray(uv_loc);
    }

    // Draw
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
    glDrawElements(GL_TRIANGLES, _vertexIndices.size(), GL_UNSIGNED_INT, 0);

    if (p_loc != -1)
        glDisableVertexAttribArray(p_loc);
    if (n_loc != -1)
        glDisableVertexAttribArray(n_loc);
    if (uv_loc != -1)
        glDisableVertexAttribArray(uv_loc);

    // Unbind buffers
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    glCheckError();
}

void TriangleMesh::init(int nTriangles, const Eigen::Vector3i *vertexIndices, int nVertices,
                        const Eigen::Vector3f *positions,
                        const Eigen::Vector3f *normals,
                        const Eigen::Vector2f *texCoords,
                        const Eigen::Matrix4f &objectToWorld)
{
    _objectToWorld = objectToWorld;

    _vertexIndices.resize(nTriangles, 3);
    for (int i = 0; i < nTriangles; ++i)
        _vertexIndices.row(i) = *(vertexIndices + i);

    _positions.resize(nVertices, 3);
    for (int i = 0; i < nVertices; ++i)
        _positions.row(i) = *(positions + i);

    if (normals)
    {
        _normals.resize(nVertices, 3);
        for (int i = 0; i < nVertices; ++i)
            _normals.row(i) = *(normals + i);
    }
    if (texCoords)
    {
        _texCoords.resize(nVertices, 2);
        for (int i = 0; i < nVertices; ++i)
            _texCoords.row(i) = *(texCoords + i);
    }
}

void TriangleMesh::initGLBuffers()
{
    glGenVertexArrays(1, &_vao);
    glGenBuffers(1, &_ebo);
    glGenBuffers(1, &_pbo);
    glGenBuffers(1, &_nbo);
    glGenBuffers(1, &_tbo);
}

void TriangleMesh::updateGLBuffers()
{
    glBindVertexArray(_vao);

    glBindBuffer(GL_ARRAY_BUFFER, _pbo);
    glBufferData(GL_ARRAY_BUFFER, _positions.size() * sizeof(GLfloat), _positions.data(), GL_STATIC_DRAW);

    // Bind other vertex attributes if present
    if (_normals.rows())
    {
        glBindBuffer(GL_ARRAY_BUFFER, _nbo);
        glBufferData(GL_ARRAY_BUFFER, _normals.size() * sizeof(GLfloat), _normals.data(), GL_STATIC_DRAW);
    }

    if (_texCoords.rows())
    {
        glBindBuffer(GL_ARRAY_BUFFER, _tbo);
        glBufferData(GL_ARRAY_BUFFER, _texCoords.size() * sizeof(GLfloat), _texCoords.data(), GL_STATIC_DRAW);
    }

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, _vertexIndices.size() * sizeof(GLuint), _vertexIndices.data(), GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    // Unbind buffers
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void TriangleMesh::deleteGLBuffers()
{
    glDeleteBuffers(1, &_ebo);
    glDeleteBuffers(1, &_pbo);
    glDeleteBuffers(1, &_nbo);
    glDeleteBuffers(1, &_tbo);
    glDeleteVertexArrays(1, &_vao);
}
