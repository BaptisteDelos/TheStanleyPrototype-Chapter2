#ifndef SHADER_H
#define SHADER_H

#include <eigen3/Eigen/Core>

#include "opengl.h"

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>


using Vector2f = Eigen::Vector2f;
using Vector3f = Eigen::Vector3f;
using Vector4f = Eigen::Vector4f;
using Matrix2f = Eigen::Matrix2f;
using Matrix3f = Eigen::Matrix3f;
using Matrix4f = Eigen::Matrix4f;


class Shader {
public:
    GLuint _glID;

    Shader();
    ~Shader();

    bool loadFromFiles(const std::string &vertexPath, const std::string &fragmentPath);
    bool load(const GLchar *vertexCode, const GLchar *fragmentCode);

    void activate();
    void deactivate();

    void setFloat(const std::string &name, const float value) const;
    void setBool(const std::string &name, const bool value) const;
    void setInt(const std::string &name, const int value) const;
    void setVec2(const std::string &name, const Vector2f &vec) const;
    void setVec3(const std::string &name, const Vector3f &vec) const;
    void setVec4(const std::string &name, const Vector4f &vec) const;
    void setMat2(const std::string &name, const Matrix2f &mat) const;
    void setMat3(const std::string &name, const Matrix3f &mat) const;
    void setMat4(const std::string &name, const Matrix4f &mat) const;

    GLint getUniformLocation(const char *name);
    GLint getAttribLocation(const char *name);

private:
    std::string _name;

};

#endif //SHADER_H
