#include "opengl.h"
#include "scene.h"
#include "mainscene.h"

#include <SOIL2.h>
#include <iostream>


/* ******************************** *
 *         Global Variables         *
 * ******************************** */

Scene *scene;

int SCENE_WIDTH = 1000;
int SCENE_HEIGHT = 1000;

int g_pixel_ratio = 1;


/* ******************************** *
 *        Callback functions        *
 * ******************************** */

static void error_callback(int error, const char *description) {
    fputs(description, stderr);
}

static void char_callback(GLFWwindow *window, unsigned int key) {
    scene->keyboard(key);
}

static void scroll_callback(GLFWwindow *window, double x, double y) {
    scene->mousescroll(x, y);
}

static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        delete scene;
        glfwWindowShouldClose(window);
        glfwDestroyWindow(window);
        glfwTerminate();

        exit(EXIT_SUCCESS);
    }
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
    scene->mousepressed(window, button, action, mods);
}

void cursorPos_callback(GLFWwindow* /*window*/, double x, double y) {
    scene->mousemove(x * g_pixel_ratio, y * g_pixel_ratio);
}

void reshape_callback(GLFWwindow* window, int width, int height) {
    scene->resize(width, height);
    scene->draw();
    glfwSwapBuffers(window);
}

void install_callbacks(GLFWwindow *window) {
    glfwSetKeyCallback(window, key_callback);
    glfwSetCharCallback(window, char_callback);
    glfwSetFramebufferSizeCallback(window, reshape_callback);
    glfwSetCursorPosCallback(window, cursorPos_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetScrollCallback(window, scroll_callback);
}



/* ******************************** *
 *       GLFW framework init        *
 * ******************************** */

GLFWwindow * initGLFW(int width, int height) {
    if (!glfwInit()) {
        fprintf(stderr, "Failed to initialize GLFW\n");
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, (GLint) GL_TRUE);
//    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // Mac OSX
    GLFWwindow *window = glfwCreateWindow(SCENE_WIDTH, SCENE_HEIGHT, "Stanley", NULL, NULL);

    GLFWimage icons[1];
    icons[0].pixels = SOIL_load_image(DATA_DIR"/images/stanley_new_logo.jpg", &icons[0].width, &icons[0].height, 0, SOIL_LOAD_RGBA);
    glfwSetWindowIcon(window, 1, icons);
    SOIL_free_image_data(icons[0].pixels);

    if (!window) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(window);
    glbinding::Binding::initialize((glbinding::GetProcAddress) glfwGetProcAddress);
    std::string glfw_version{ glfwGetVersionString() };
    std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLSL version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    std::cout << "GLFW version: " << glfw_version << std::endl;

    int w, h;
    glfwGetFramebufferSize(window, &w, &h);

    if (w == 2 * SCENE_WIDTH)
        g_pixel_ratio = 2;

    return window;
}


int main (int argc, char **argv) {
    glfwSetErrorCallback(error_callback);

    GLFWwindow *window = initGLFW(SCENE_WIDTH, SCENE_HEIGHT);
    int w, h;
    glfwGetFramebufferSize(window, &w, &h);
    scene = new MainScene(SCENE_WIDTH, SCENE_HEIGHT);
    install_callbacks(window);

    glCheckError();

    double t0 = glfwGetTime();
    double t1 = t0;

    while (!glfwWindowShouldClose(window))
    {
        // Render the scene
        t1 = glfwGetTime();
        if((t1 - t0) > 0.04) {
            scene->draw();
            glfwSwapBuffers(window);
            t0 = t1;
        }
        glfwPollEvents();

        glCheckError();
    }

    delete scene;

    glfwDestroyWindow(window);
    glfwTerminate();

    exit(EXIT_SUCCESS);
}
