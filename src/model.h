#ifndef MODEL_H
#define MODEL_H

#include "trianglemesh.h"

class Model
{
public:
    Model();
    ~Model();

    void draw(Shader shader);
    void addMesh(TriangleMesh *mesh);

    void translate(Eigen::Vector3f t);
    void rotate(float angle, Eigen::Vector3f axis);
    void scale(Eigen::Vector3f s);

    Eigen::Matrix4f localTransform() const;

private:
    std::vector<std::unique_ptr<TriangleMesh>>                  _meshes;

    Eigen::Transform<float, 3, Eigen::Affine, Eigen::RowMajor>  _localTransform;

};

#endif // MODEL_H
