#ifndef TRIANGLE_MESH_H
#define TRIANGLE_MESH_H

#include <Eigen/Eigen>

#include "opengl.h"
#include "shader.h"

class TriangleMesh
{
public:
    TriangleMesh();
    TriangleMesh(int nTriangles, const Eigen::Vector3i *vertexIndices, int nVertices,
                 const Eigen::Vector3f *positions,
                 const Eigen::Vector3f *normals=nullptr,
                 const Eigen::Vector2f *texCoords=nullptr,
                 const Eigen::Matrix4f &objectToWorld=Eigen::Matrix4f::Identity());
    TriangleMesh(const TriangleMesh&);
    TriangleMesh(const std::string& filename);

    ~TriangleMesh();

    void draw(Shader &shader);

protected:
    Eigen::Matrix4f                                             _objectToWorld;

    // Vertex attributes
    Eigen::Matrix<int,   Eigen::Dynamic, 3, Eigen::RowMajor>    _vertexIndices;
    Eigen::Matrix<float, Eigen::Dynamic, 3, Eigen::RowMajor>    _positions;
    Eigen::Matrix<float, Eigen::Dynamic, 3, Eigen::RowMajor>    _normals;
    Eigen::Matrix<float, Eigen::Dynamic, 2, Eigen::RowMajor>    _texCoords;

    // OpenGL attribute buffers
    GLuint                                                      _vao;
    GLuint                                                      _ebo;
    GLuint                                                      _pbo;
    GLuint                                                      _nbo;
    GLuint                                                      _tbo;

    void init(int nTriangles, const Eigen::Vector3i *vertexIndices, int nVertices,
              const Eigen::Vector3f *positions,
              const Eigen::Vector3f *normals=nullptr,
              const Eigen::Vector2f *texCoords=nullptr,
              const Eigen::Matrix4f &objectToWorld=Eigen::Matrix4f::Identity());

    void initGLBuffers();
    void updateGLBuffers();
    void deleteGLBuffers();

private:
    // Delete copy assignment operator to prevent possible GL objects invalidation
    TriangleMesh& operator=(const TriangleMesh&) = delete;
};

#endif // TRIANGLE_MESH_H
