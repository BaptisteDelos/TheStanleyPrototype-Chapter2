#include "shader.h"

#include <algorithm>

Shader::Shader()
{

}

Shader::~Shader()
{

}

bool Shader::loadFromFiles(const std::string &vertexPath, const std::string &fragmentPath)
{
    std::string vertexCode;
    std::string fragmentCode;
    std::ifstream vshaderFile;
    std::fstream fshaderFile;

    vshaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    fshaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    std::string fp{fragmentPath};
    size_t pos = 0;
    std::string token;
    while ((pos = fp.find("/")) != std::string::npos)
    {
        token = fp.substr(0, pos);
        fp.erase(0, pos + 1);
    }
    _name = fp.substr(0, fp.find("."));
    std::transform(_name.begin(), _name.end(), _name.begin(), ::toupper);

    try {
        vshaderFile.open(vertexPath.c_str());
        fshaderFile.open(fragmentPath.c_str());

        std::stringstream vss, fss;

        vss << vshaderFile.rdbuf();
        fss << fshaderFile.rdbuf();

        vshaderFile.close();
        fshaderFile.close();

        vertexCode = vss.str();
        fragmentCode = fss.str();
    } catch(std::ifstream::failure e) {
        std::cerr << "ERROR::SHADER::" << _name << "::FILE_NOT_SUCCESSFULLY_READ" << std::endl;
    }
    const char* vshaderCode = vertexCode.c_str();
    const char* fshaderCode = fragmentCode.c_str();

    return load(vshaderCode, fshaderCode);
}

bool Shader::load(const GLchar *vertexCode, const GLchar *fragmentCode)
{
    GLuint vertex, fragment;
    int success;
    char infolog[512];

    // vertex shader
    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vertexCode, NULL);
    glCompileShader(vertex);

    glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        glGetShaderInfoLog(vertex, 512, NULL, infolog);
        std::cerr << "ERROR::SHADER::" << _name << "::VERTEX::COMPILATION_FAILED\n" << infolog << std::endl;

        return false;
    };

    // fragment shader
    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fragmentCode, NULL);
    glCompileShader(fragment);

    glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        glGetShaderInfoLog(fragment, 512, NULL, infolog);
        std::cerr << "ERROR::SHADER::" << _name << "::FRAGMENT::COMPILATION_FAILED\n" << infolog << std::endl;

        return false;
    };

    // shader program
    _glID = glCreateProgram();
    glAttachShader(_glID, vertex);
    glAttachShader(_glID, fragment);
    glLinkProgram(_glID);

    glGetProgramiv(_glID, GL_LINK_STATUS, &success);
    if(!success)
    {
        glGetProgramInfoLog(_glID, 512, NULL, infolog);
        std::cerr << "ERROR::SHADER::" << _name << "::PROGRAM::LINKING_FAILED\n" << infolog << std::endl;

        return false;
    }

// delete the shaders as they're linked into our program now and no longer necessery
    glDeleteShader(vertex);
    glDeleteShader(fragment);

    return true;
}

void Shader::activate()
{
    glUseProgram(_glID);
}

void Shader::deactivate()
{
    glUseProgram(0);
}

void Shader::setFloat(const std::string &name, const float value) const{
    glUniform1f(glGetUniformLocation(_glID, name.c_str()), value);
}

void Shader::setBool(const std::string &name, const bool value) const{
    glUniform1i(glGetUniformLocation(_glID, name.c_str()), value);
}

void Shader::setInt(const std::string &name, const int value) const{
    glUniform1i(glGetUniformLocation(_glID, name.c_str()), value);
}

void Shader::setVec2(const std::string &name, const Vector2f &vec) const{
    glUniform2fv(glGetUniformLocation(_glID, name.c_str()), 1, &vec[0]);
}

void Shader::setVec3(const std::string &name, const Vector3f &vec) const{
    glUniform3fv(glGetUniformLocation(_glID, name.c_str()), 1, &vec[0]);
}

void Shader::setVec4(const std::string &name, const Vector4f &vec) const{
    glUniform4fv(glGetUniformLocation(_glID, name.c_str()), 1, &vec[0]);
}

void Shader::setMat2(const std::string &name, const Matrix2f &mat) const{
    glUniformMatrix2fv(glGetUniformLocation(_glID, name.c_str()), 1, GL_FALSE, mat.data());
}

void Shader::setMat3(const std::string &name, const Matrix3f &mat) const{
    glUniformMatrix3fv(glGetUniformLocation(_glID, name.c_str()), 1, GL_FALSE, mat.data());
}

void Shader::setMat4(const std::string &name, const Matrix4f &mat) const {
    glUniformMatrix4fv(glGetUniformLocation(_glID, name.c_str()), 1, GL_FALSE, mat.data());
}

GLint Shader::getUniformLocation(const char *name)
{
    return glGetUniformLocation(_glID, name);
}

GLint Shader::getAttribLocation(const char *name)
{
    return glGetAttribLocation(_glID, name);
}
