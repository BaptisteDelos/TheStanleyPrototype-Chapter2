#include <model.h>

#include <utility>

#include <eigen3/Eigen/Geometry>

Model::Model() : _localTransform(Eigen::Matrix4f::Identity())
{

}

Model::~Model()
{

}

void Model::draw(Shader shader)
{
    for (auto& mesh_ptr : _meshes)
        mesh_ptr->draw(shader);
}


void Model::addMesh(TriangleMesh *mesh)
{
    assert(mesh);

    _meshes.emplace_back(std::unique_ptr<TriangleMesh>(mesh));
}

void Model::translate(Eigen::Vector3f t)
{
    _localTransform.translate(t);
}

void Model::rotate(float angle, Eigen::Vector3f axis)
{
    _localTransform.rotate(Eigen::AngleAxisf(angle, axis));
}

void Model::scale(Eigen::Vector3f s)
{
    _localTransform.scale(s);
}

Eigen::Matrix4f Model::localTransform() const
{
    return _localTransform.matrix();
}
