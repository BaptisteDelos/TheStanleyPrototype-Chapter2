#version 410 core

in vec3 Normal;

out vec4 FragColor;

void main()
{
    FragColor = vec4(clamp(dot(normalize(Normal), vec3(0., 0., 1.)), 0., 1.) * vec3(0., 0., 1.), 1.0);
}
