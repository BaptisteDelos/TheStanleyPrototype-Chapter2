#version 410 core

layout (location = 0) in vec3 iposition;
layout (location = 1) in vec3 inormal;
layout (location = 2) in vec3 icolor;
layout (location = 3) in vec2 itexcoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 FragPos;
out vec3 Normal;
out vec3 Color;
out vec2 TexCoords;

void main()
{
    gl_Position = projection * view * model * vec4(iposition, 1.0f);

    FragPos = iposition;
    Normal = inormal;
    Color = icolor;
    TexCoords = itexcoord;
}
